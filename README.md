# springboot_mybatis多租户

#### 介绍

一种参考mybatis-plus多租户方案，在springboot,mybatis项目中实现demo

#### 软件架构

1.多租户代码完全抄了mybatis-plus多租户代码
2.后端拦截器通过请求头中的cityId实现多租户

#### 安装教程

1.  启动服务
2.  访问 127.0.0.1:9090/getUsers ，127.0.0.1:9090/getUserAddressList 这两个接口的时候，需要在请求头中添加 cityId

#### 参与贡献

1.  Fork 本仓库

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
