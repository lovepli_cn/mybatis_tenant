

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `age` int(3) NOT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `city_id` int(11) NULL DEFAULT 0 COMMENT '城市id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 108, '2019-09-28 21:12:31', '成都', 1);
INSERT INTO `user` VALUES (2, 18, '2019-09-28 21:13:12', '北京', 2);
INSERT INTO `user` VALUES (3, 25, '2020-11-16 09:57:13', '深圳', 3);

SET FOREIGN_KEY_CHECKS = 1;
